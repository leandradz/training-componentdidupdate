import { Component } from "react";
import "./App.css";
import CharacterList from "./components/CharacterList";

class App extends Component {
  state = {
    characterList: [],
    nextUrl: "https://rickandmortyapi.com/api/character/",
  };

  getCharacters(url) {
    fetch(url)
      .then((response) => response.json())
      .then((response) =>
        this.setState({
          characterList: [...this.state.characterList, ...response.results],
          nextUrl: response.info.next,
        })
      );
  }

  componentDidMount() {
    this.getCharacters(this.state.nextUrl);
  }

  componentDidUpdate(_, prevState) {
    if (prevState !== null) {
      this.getCharacters(this.state.nextUrl);
    }
  }

  render() {
    const { characterList } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <CharacterList list={characterList} />
        </header>
      </div>
    );
  }
}

export default App;
