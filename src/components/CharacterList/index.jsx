import { Component } from "react";
import Character from "../Character";

class CharacterList extends Component {
  render() {
    const { list } = this.props;
    return list.map((item) => {
      return (
        <Character name={item.name} img={item.image} species={item.species} />
      );
    });
  }
}

export default CharacterList;
