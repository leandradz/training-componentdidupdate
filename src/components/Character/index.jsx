import { Component } from "react";
import "./styles.css";

class Character extends Component {
  render() {
    const { name, img, species } = this.props;
    return (
      <div className="character">
        <h1>{name}</h1>
        <img className="picture" src={img} alt={name}></img>
        <h2>{species}</h2>
      </div>
    );
  }
}

export default Character;
